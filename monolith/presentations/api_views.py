from django.http import JsonResponse
from .models import Presentation
from events.models import Conference
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
import json
from events.api_views import ConferenceListEncoder
import pika


class PresentationListEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "title",
        "status",
    ]

    def get_extra_data(self, o):
        return {"status": o.status.name}



class PresentationDetailEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "presenter_name",
        "company_name",
        "presenter_email",
        "title",
        "synopsis",
        "created",
        "status",
        "conference",
    ]
    encoders = {
        "conference": ConferenceListEncoder(),
    }

    def get_extra_data(self, o):
        return {"status": o.status.name}


@require_http_methods(["GET", "POST"])
def api_list_presentations(request, conference_id):
    if request.method == "GET":
        presentations = [
            {
                "title": p.title,
                "status": p.status.name,
                "href": p.get_api_url(),
            }
            for p in Presentation.objects.filter(conference=conference_id)
        ]
        return JsonResponse({"presentations": presentations})

    else:
        content = json.loads(request.body)

        try:
            conference = Conference.objects.get(id=conference_id)
            content["conference"] = conference
        except Conference.DoesNotExist:
            return JsonResponse(
                {"message": "Conference does not exist"},
            )

        presentation = Presentation.create(**content)
        return JsonResponse(
            presentation,
            encoder=PresentationDetailEncoder,
            safe=False,
        )


# @require_http_methods(["GET", "DELETE", "PUT"])
def api_show_presentation(request, id):
    presentation = Presentation.objects.get(id=id)
    return JsonResponse(
        presentation,
        encoder=PresentationDetailEncoder,
        safe=False,
    )

    # if request.method == "GET":
    #     presentation = Presentation.objects.get(id=id)
    #     return JsonResponse(
    #         presentation,
    #         encoder=PresentationDetailEncoder,
    #         safe=False,
    #     )

    # elif request.method == "DELETE":
    #     count, _ = Presentation.objects.filter(id=id).delete()
    #     return JsonResponse({"deleted": count > 0})

    # else:
    #     content = json.loads(request.body)

    #     Presentation.objects.filter(id=id).update(**content)

    #     presentation = Presentation.objects.get(id=id)
    #     return JsonResponse(
    #         presentation,
    #         encoder=PresentationDetailEncoder,
    #         safe=False,
    #     )


def process_message(queue_name, message):
    parameters = pika.ConnectionParameters(host="rabbitmq")
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()
    channel.queue_declare(queue=queue_name)
    channel.basic_publish(
        exchange="",
        routing_key=queue_name,
        body=json.dumps(message),
    )
    connection.close()


@require_http_methods(["PUT"])
def api_approve_presentation(request, id):
    presentation = Presentation.objects.get(id=id)
    presentation.approve_status()

    message = {
        "presenter_name": presentation.presenter_name,
        "presenter_email": presentation.presenter_email,
        "title": presentation.title
    }
    process_message("presentation_approvals", message)

    return JsonResponse(
        presentation,
        encoder=PresentationDetailEncoder,
        safe=False,
    )


@require_http_methods(["PUT"])
def api_reject_presentation(request, id):
    presentation = Presentation.objects.get(id=id)
    presentation.reject_status()

    message = {
        "presenter_name": presentation.presenter_name,
        "presenter_email": presentation.presenter_email,
        "title": presentation.title
    }
    process_message("presentation_rejections", message)

    return JsonResponse(
        presentation,
        encoder=PresentationDetailEncoder,
        safe=False,
    )
