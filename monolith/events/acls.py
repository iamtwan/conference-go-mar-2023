import requests
import json
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_photo(city, state):
    url = "https://api.pexels.com/v1/search"
    headers = {"Authorization": PEXELS_API_KEY}
    params = {"per_page": 1, "query": city + " " + state}
    response = requests.get(url, params=params, headers=headers)
    content = json.loads(response.content)
    photos = content["photos"]

    try:
        return {"url": "null"} if len(photos) == 0 else {"picture_url": photos[0]["src"]["original"]}
    except (KeyError, IndexError):
        return {"picture_url": None}


def get_weather_data(city, state):
    url_coords = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},US&appid={OPEN_WEATHER_API_KEY}"
    coords_response = requests.get(url_coords)
    coor_search = coords_response.json()

    try:
        if len(coor_search) == 0:
            return {"weather": None}
    except (IndexError):
        return {"City or State": None}

    lat = coor_search[0]["lat"]
    lon = coor_search[0]["lon"]

    url_weather = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&units=imperial&appid={OPEN_WEATHER_API_KEY}"
    weather_response = requests.get(url_weather)
    weather_search = weather_response.json()

    try:
        if len(weather_search["weather"]) == 0:
            return {"weather": None}
    except (KeyError):
        return {"weather": None}
    temp = weather_search["main"]["temp"]
    description = weather_search["weather"][0]["description"]
    return {"weather": {"description": description, "temperature": temp}}
